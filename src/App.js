import "./App.css";
import { useState } from "react";
import { FruitList } from "./components/FruitList";

function App() {
  const [fruits, setFruits] = useState([
    { name: "banana", color: "yellow", price: 2 },
    { name: "cherry", color: "red", price: 3 },
    { name: "strawberry", color: "red", price: 4 },
  ]);

  function filterRedFruits() {
    let frutasvermelhas = fruits.filter((elem) => elem.color === "red");
    setFruits(frutasvermelhas);
  }

  return (
    <div className="App">
      <header className="App-header">
        <FruitList frutas={fruits} />
        <button onClick={filterRedFruits}>Mostrar frutas vermelhas</button>
      </header>
    </div>
  );
}

export default App;
