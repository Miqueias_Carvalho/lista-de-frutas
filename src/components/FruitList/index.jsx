export function FruitList({ frutas }) {
  const totalPrice = frutas.reduce((vAnt, vAtual) => {
    return vAnt + vAtual.price;
  }, 0);

  console.log("array frutas: ", frutas, "preço atual: ", totalPrice);

  return (
    <div>
      <h2>Preço Total = {totalPrice}</h2>
      <ul>
        {frutas.map((fruta, index) => (
          <li key={index}>{fruta.name}</li>
        ))}
      </ul>
    </div>
  );
}

/*export default FruitList */
